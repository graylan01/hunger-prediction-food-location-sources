import sqlite3
import os

def convert_db_to_md(db_file):
    # Connect to the SQLite database
    conn = sqlite3.connect(db_file)
    cursor = conn.cursor()

    # Get the list of tables in the database
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
    tables = cursor.fetchall()

    # Create a directory to store Markdown files
    md_dir = "markdown_output"
    os.makedirs(md_dir, exist_ok=True)

    # Iterate over each table and convert it to Markdown
    for table in tables:
        table_name = table[0]
        cursor.execute(f"SELECT * FROM {table_name};")
        rows = cursor.fetchall()

        # Write table data to a Markdown file
        md_file = os.path.join(md_dir, f"{table_name}.md")
        with open(md_file, "w") as f:
            # Write table name as header
            f.write(f"# {table_name}\n\n")

            # Write column names as table headers
            column_names = [description[0] for description in cursor.description]
            f.write("| " + " | ".join(column_names) + " |\n")
            f.write("| " + " | ".join(["---"] * len(column_names)) + " |\n")

            # Write table rows
            for row in rows:
                f.write("| " + " | ".join(str(cell) for cell in row) + " |\n")

    print("Conversion completed. Markdown files saved in the 'markdown_output' directory.")

    # Close the database connection
    conn.close()

# Provide the path to the SQLite database file
db_file_path = "path/to/your/database.db"

# Call the function to convert the SQLite database to Markdown
convert_db_to_md(db_file_path)